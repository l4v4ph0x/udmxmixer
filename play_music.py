"""

Play a file continously, and exit gracefully on signal
Based on https://github.com/steveway/papagayo-ng/blob/working_vol/SoundPlayer.py
@author Guy Sheffer (GuySoft) <guysoft at gmail dot com>

modified by: lava phox

"""
import signal
import time
import os
import threading
import pyaudio
from pydub import AudioSegment
from pydub.utils import make_chunks
import numpy as np

from pyudmx import pyudmx
from time import sleep

import _thread

"""
flash_lights = False
flash_lights_second = False

def print_time():
    global flash_lights, flash_lights_second

    # Channel value list for channels 1-(3 * 5) because were running 5 rgbs
    cv = [0 for v in range(0, 3 * 5)]

    # Create an instance of the DMX controller and open it
    print("Opening DMX controller...")
    dev = pyudmx.uDMXDevice()
    # This will automagically find a single Anyma-type USB DMX controller
    dev.open()
    # For informational purpose, display what we know about the DMX controller
    print(dev.Device)


    until_strobe_count = 0
    until_strobe = 20
    until_strobe_blue = 10
    strobe_sleep = 500 # in ms

    while True:
        if flash_lights == True:
            stobe_value = 255 if until_strobe_count % 2 == 0 else 0

            cv[6] = 255 if until_strobe_count < until_strobe else stobe_value  # red

            if flash_lights_second == True:
                cv[7] = 255 if until_strobe_count < until_strobe else stobe_value  # green
            else:
                cv[7] = 0  # green

            cv[2] = 0 if until_strobe_count < until_strobe_blue else stobe_value # blue
            cv[5] = 0 if until_strobe_count < until_strobe_blue else stobe_value # blue
            cv[8] = 0 if until_strobe_count < until_strobe_blue else stobe_value # blue
            cv[11] = 0 if until_strobe_count < until_strobe_blue else stobe_value # blue
            cv[14] = 0 if until_strobe_count < until_strobe_blue else stobe_value # blue

            try:
                dev.send_multi_value(23, cv)
            except:
                print("overload")

            if until_strobe_count < until_strobe:
                time.sleep(0.03)

            cv[3] = 255 if until_strobe_count < until_strobe else stobe_value  # red
            cv[9] = 255 if until_strobe_count < until_strobe else stobe_value  # red

            if flash_lights_second == True:
                cv[4] = 255 if until_strobe_count < until_strobe else stobe_value  # green
                cv[10] = 255 if until_strobe_count < until_strobe else stobe_value  # green
            else:
                cv[4] = 0  # green
                cv[10] = 0  # green

            try:
                dev.send_multi_value(23, cv)
            except:
                print("overload")

            if until_strobe_count < until_strobe:
                time.sleep(0.03)

            cv[0] = 255 if until_strobe_count < until_strobe else stobe_value  # red
            cv[12] = 255 if until_strobe_count < until_strobe else stobe_value  # red

            if flash_lights_second == True:
                cv[1] = 255 if until_strobe_count < until_strobe else stobe_value # green
                cv[13] = 255 if until_strobe_count < until_strobe else stobe_value # green
            else:
                cv[1] = 0  # green
                cv[13] = 0  # green

            try:
                dev.send_multi_value(23, cv)
            except:
                print("overload")

            if not until_strobe_count < until_strobe:
                time.sleep(strobe_sleep / 1000.0)

                if strobe_sleep > 50:
                    strobe_sleep -= strobe_sleep / 5.0

            until_strobe_count += 1

        else:
            cv = [0 for v in range(0, 3 * 5)]
            try:
                dev.send_multi_value(23, cv)
            except:
                print("overload")

            time.sleep(0.01)
            until_strobe_count = 0

"""

class GracefulKiller:

    def __init__(self):
        self.kill_now = False

        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True


class Player(threading.Thread):
    """
    A simple class based on PyAudio and pydub to play.
    """

    def __init__(self, filepath):
        """
        Initialize `Player` class.

        PARAM:
            -- filepath (String) : File Path to wave file.
        """
        super(Player, self).__init__()
        self.filepath = os.path.abspath(filepath)
        self.peak = 0
        self.bars = 0
        self.time = -1
        self.length = 0
        self.paused = False
        self.end = False

    def run(self):
        global flash_lights, flash_lights_second

        # Open an audio segment
        print(self.filepath)
        sound = AudioSegment.from_file(self.filepath)
        player = pyaudio.PyAudio()

        stream = player.open(format = player.get_format_from_width(sound.sample_width),
            channels = sound.channels,
            rate = sound.frame_rate,
            output = True)

        # PLAYBACK
        start = 0
        self.length = sound.duration_seconds
        volume = 100.0
        playchunk = sound[start*1000.0:(start + self.length)*1000.0] - (60 - (60 * (volume/100.0)))
        millisecondchunk = 50 / 1000.0

        self.time = start
        for chunks in make_chunks(playchunk, millisecondchunk * 1000):
            if self.time == -1:
                break

            self.time += millisecondchunk
            stream.write(chunks._data)

            audio_data = np.fromstring(chunks._data, dtype=np.short)
            self.peak = np.average(np.abs(audio_data)) *2
            self.bars = int(50 * self.peak / 2 ** 16)

            if self.time >= start + self.length:
                break

            while self.paused:
                time.sleep(0.1)

        self.end = True
        stream.close()

    def play(self):
        """
        Just another name for self.start()
        """
        self.start()

    def unpause(self):
        self.paused = False

    def pause(self):
        self.paused = True

    def stop(self):
        """
        Stop playback.
        """
        self.time = -1

class play_music:
    """
    Lil class to play music in background.
    """

    def __init__(self):
        self.killer = GracefulKiller()
        self.player = None
        self.audio_file = ""

    def pause(self):
        if self.player != None:
            self.player.pause()

    def stop(self):
        if self.player != None:
            self.player.stop()
            self.Player = None

    def is_playing(self):
        if self.player == None:
            return False

        return not self.player.end and self.player.time > 0

    def thread_play(self, audio_file):
        """
        thread that plays music in background until its stoped or ended
        """
        self.player = Player(audio_file)
        self.player.play()

        while self.player.end == False:
            time.sleep(0.1)
            if self.killer.kill_now:
                break

        self.player.stop()
        self.player = None

        print(f"{__file__} - play_music.class -> thread_play: stopped `{audio_file}`")

    def play(self, audio_file):
        """
        Play audio file in the background, accept a SIGINT or SIGTERM to stop.
        """
        print(f"{__file__} - play_music.class -> play({audio_file})")

        # check if we want to play another tract were already playing
        if self.audio_file != audio_file:
            self.audio_file = audio_file

            # if we have player already playing then stop it and sleep a little
            if self.player != None:
                self.player.stop()
                self.player = None

        if self.is_playing():
            self.player.unpause()
        else:
            _thread.start_new_thread(self.thread_play, (audio_file, ))


#if __name__ == '__main__':
#    _thread.start_new_thread (print_time, () )
#    play_audio_background("audio.wav")
