import sys
from pyudmx import pyudmx

class uDMX:

    def __init__(self, channels = 512):
        self.channels = channels

        try:
            # Create an instance of the DMX controller and open it
            print("Opening DMX controller...")
            self.dev = pyudmx.uDMXDevice()

            # This will automagically find a single Anyma-type USB DMX controller
            self.dev.open()

            # For informational purpose, display what we know about the DMX controller
            print(self.dev.Device)
        except Exception:
            print("no device found")

        # Channel value list for channels 1-(3 * 5) because were running 5 rgbs
        self.cvs = {}
        self.cvs["off"] = [0 for v in range(0, channels)]

        self.start_address = 0

    def close(self):
        if self.dev is not None:
            self.dev.close()

    def set_start_address(self, start_address, debug = True):
        if debug:
            print(f"{__file__} - set_start_address({start_address})")
        self.start_address = start_address

    def set_channel_value(self, id, channel, value, debug = True):
        if debug:
            print(f"{__file__} - set_channel_value({id}, {channel}, {value})")

        if id not in list(self.cvs.keys()):
            self.cvs[id] = [0 for v in range(0, self.channels)]

        self.cvs[id][channel] = value

    def send(self, id, debug = True):
        if debug:
            print(f"{__file__} - send({id})")

        try:
            self.dev.send_multi_value(self.start_address, self.cvs[id])
        except:
            if debug:
                print("overload")
