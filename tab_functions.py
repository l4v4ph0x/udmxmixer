import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import *

from file_frame import file_frame

class tab_functions:

    def __init__(self, window, ui):
        print(f"{__file__} - __init__({window}, {ui})")

        # set table widget headers
        ui.tw_functionDetails.setColumnCount(4)
        ui.tw_functionDetails.setHorizontalHeaderLabels(["start address", "channels", "values", "time to hold (in ms)"])

        # set functions tab signals

        ui.btn_addFunction.clicked.connect(lambda: self.btn_addFunction_clicked(window, ui))
        ui.btn_removeFunction.clicked.connect(lambda: self.btn_removeFunction_clicked(window, ui))
        ui.lw_functions.itemClicked.connect(lambda e: self.lw_functions_itemClicked(window, ui, e))
        ui.tw_functionDetails.cellClicked.connect(lambda r, c: self.tw_functionDetails_cellClicked(window, ui, r, c))

        ui.te_functionName.textChanged.connect(lambda: self.te_functionName_textChanged(window, ui))
        ui.te_editCell.textChanged.connect(lambda: self.te_editCell_textChanged(window, ui))

        ui.btn_addLine.clicked.connect(lambda: self.btn_addLine_clicked(window, ui))
        ui.btn_removeLastLine.clicked.connect(lambda: self.btn_removeLastLine_clicked(window, ui))
        ui.btn_saveFunction.clicked.connect(lambda: self.btn_saveFunction_clicked(window, ui))

        # add function names to lw_functions
        for function in file_frame().get_functions():
            ui.lw_functions.insertItem(0, QListWidgetItem(function["name"]))

        # define local vars
        self.selected_function_name = ""
        self.selected_function_cell = None

    def btn_addFunction_clicked(self, window, ui):
        print(f"{__file__} - btn_addFunction_clicked({window}, {ui})")

        # get unique function name
        for n in range(1, 100):
            if f"New Function {n}" not in [f["name"] for f in file_frame().get_functions()]:

                # add new function
                file_frame().add_new_function(f"New Function {n}")

                # add default function line also
                file_frame().update_function(f"New Function {n}", {"lines": [{"start address": "23", "channels": "1, 2, 3", "values": "255, 255, 255", "time to hold": "2000"}]})

                ui.lw_functions.insertItem(0, QListWidgetItem(f"New Function {n}"))

                break

    def btn_removeFunction_clicked(self, window, ui):
        print(f"{__file__} - btn_removeFunction_clicked({window}, {ui})")

        if self.selected_function_name != "":
            file_frame().remove_function(self.selected_function_name)

            # reload function names to lw_functions
            ui.lw_functions.clear()
            for function in file_frame().get_functions():
                ui.lw_functions.insertItem(0, QListWidgetItem(function["name"]))

            # clear edit panels
            ui.te_functionName.setText("")
            ui.te_editCell.setText("")
            ui.tw_functionDetails.setRowCount(0)

            # remove self selected vars
            self.selected_function_name = ""
            self.selected_function_cell = None

            self.disable_edit_function(window, ui)
        else:
            print(f"ERROR - {__file__} function havent selected")

    def lw_functions_itemClicked(self, window, ui, item):
        print(f"{__file__} - lw_functions_itemClicked({window}, {ui}, {item})")
        self.selected_function_name = item.text()

        lines = file_frame().get_function_lines(self.selected_function_name)
        ui.tw_functionDetails.setRowCount(len(lines))
        self.enable_edit_function(window, ui)

        ui.te_functionName.setText(item.text())

        for i, line in enumerate(lines):
            ui.tw_functionDetails.setItem(i, 0, QTableWidgetItem(line["start address"]))
            ui.tw_functionDetails.setItem(i, 1, QTableWidgetItem(line["channels"]))
            ui.tw_functionDetails.setItem(i, 2, QTableWidgetItem(line["values"]))
            ui.tw_functionDetails.setItem(i, 3, QTableWidgetItem(line["time to hold"]))

    def tw_functionDetails_cellClicked(self, window, ui, row, column):
        print(f"{__file__} - tw_functionDetails_cellClicked({window}, {ui}, {row}, {column})")

        # set local var to selected cell and set edit cell text it it
        self.selected_function_cell = {"row": row, "column": column}
        ui.te_editCell.setText(ui.tw_functionDetails.item(row, column).text())

    def te_functionName_textChanged(self, window, ui):
        print(f"{__file__} - te_functionName_textChanged({window}, {ui})")

        if ui.te_functionName.toPlainText() == self.selected_function_name:
            return

        if file_frame().update_function(self.selected_function_name, {"name": ui.te_functionName.toPlainText()}):
            self.selected_function_name = ui.te_functionName.toPlainText()

            # update also list widget text
            ui.lw_functions.item(ui.lw_functions.currentRow()).setText(self.selected_function_name)

    def te_editCell_textChanged(self, window, ui):
        print(f"{__file__} - te_editCell_textChanged({window}, {ui})")

        if self.selected_function_cell != None:
            # edit here some thing more lala bla bla
            ui.tw_functionDetails.item(self.selected_function_cell["row"], self.selected_function_cell["column"]).setText(ui.te_editCell.toPlainText())

    def enable_edit_function(self, window, ui):
        print(f"{__file__} - enable_edit_function({window}, {ui})")

        ui.te_functionName.setEnabled(True)
        ui.te_editCell.setEnabled(True)
        ui.btn_addLine.setEnabled(True)
        ui.btn_removeLastLine.setEnabled(True)
        ui.btn_saveFunction.setEnabled(True)

    def disable_edit_function(self, window, ui):
        print(f"{__file__} - disable_edit_function({window}, {ui})")

        ui.te_functionName.setEnabled(False)
        ui.te_editCell.setEnabled(False)
        ui.btn_addLine.setEnabled(False)
        ui.btn_removeLastLine.setEnabled(False)
        ui.btn_saveFunction.setEnabled(False)

    def btn_addLine_clicked(self, window, ui):
        print(f"{__file__} - btn_addLine_clicked({window}, {ui})")
        ui.tw_functionDetails.setRowCount(ui.tw_functionDetails.rowCount() + 1)

        ui.tw_functionDetails.setItem(ui.tw_functionDetails.rowCount() -1, 0, QTableWidgetItem(""))
        ui.tw_functionDetails.setItem(ui.tw_functionDetails.rowCount() -1, 1, QTableWidgetItem(""))
        ui.tw_functionDetails.setItem(ui.tw_functionDetails.rowCount() -1, 2, QTableWidgetItem(""))
        ui.tw_functionDetails.setItem(ui.tw_functionDetails.rowCount() -1, 3, QTableWidgetItem(""))


    def btn_removeLastLine_clicked(self, window, ui):
        print(f"{__file__} - btn_removeLastLine_clicked({window}, {ui})")

        if ui.tw_functionDetails.rowCount() > 0:
            ui.tw_functionDetails.removeRow(ui.tw_functionDetails.rowCount() -1)
        else:
            print(f"ERROR - {__file__} tw_functionDetails rowCount is already 0")

    def btn_saveFunction_clicked(self, window, ui):
        print(f"{__file__} - btn_saveFunction_clicked({window}, {ui})")

        lines = []
        for row in range(0, ui.tw_functionDetails.rowCount()):
            line = {}

            line["start address"] = ui.tw_functionDetails.item(row, 0).text() if ui.tw_functionDetails.item(row, 0) != None else ""
            line["channels"] = ui.tw_functionDetails.item(row, 1).text() if ui.tw_functionDetails.item(row, 1) != None else ""
            line["values"] = ui.tw_functionDetails.item(row, 2).text() if ui.tw_functionDetails.item(row, 2) != None else ""
            line["time to hold"] = ui.tw_functionDetails.item(row, 3).text() if ui.tw_functionDetails.item(row, 3) != None else ""

            lines.append(line)

        file_frame().update_function(self.selected_function_name, {"lines": lines})
