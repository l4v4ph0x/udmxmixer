import json

from os import listdir
from os.path import isfile, isdir, join

class file_frame:

    def __init__(self, debug = True):
        self.debug = debug

        if self.debug:
            print(f"{__file__} - __init__()")

        # set config file
        self.config_file_name = "data.conf"

        # check if file exists, and create new if not
        if not isfile(self.config_file_name):
            with open(self.config_file_name, "w") as f:
                f.write("{}")

    def set_music_folder(self, music_folder):
        if self.debug:
            print(f"{__file__} - set_music_folder({music_folder})")

        with open(self.config_file_name, "r") as f:
            data = json.loads(f.read())
            data["music folder"] = music_folder

        with open(self.config_file_name, "w") as f:
            json.dump(data, f)

    def get_music_folder(self):
        if self.debug:
            print(f"{__file__} - get_music_folder()")

        ret = ""

        with open(self.config_file_name, "r") as f:
            data = json.loads(f.read())
            if "music folder" in list(data.keys()):
                ret = data["music folder"]
            else:
                if self.debug:
                    print(f"ERROR - {__file__} missing `music folder` key in {self.config_file_name}")
        return ret

    def add_new_function(self, function_name: str):
        if self.debug:
            print(f"{__file__} - add_new_function({function_name})")

        with open(self.config_file_name, "r") as f:
            data = json.loads(f.read())

            # first check if functions key exists
            # if not then create it
            if "functions" in list(data.keys()):

                # also check if new function name exists already
                if function_name not in [n["name"] for n in data["functions"]]:
                    data["functions"].append({"name": function_name, "lines": []})
                else:
                    if self.debug:
                        print(f"ERROR - {__file__} function `{function_name}` already exists")
            else:
                data["functions"] = [{"name": function_name, "lines": []}]

        with open(self.config_file_name, "w") as f:
            json.dump(data, f)

    def remove_function(self, function_name: str):
        if self.debug:
            print(f"{__file__} - remove_function({function_name})")

        with open(self.config_file_name, "r") as f:
            data = json.loads(f.read())

            # always check if functions key exists
            if "functions" in list(data.keys()):
                for i, function in enumerate(data["functions"]):
                    if function["name"] == function_name:
                        data["functions"].pop(i)
                        break
            else:
                if self.debug:
                    print(f"ERROR - {__file__} missing `functions` key in {self.config_file_name}")

        with open(self.config_file_name, "w") as f:
            json.dump(data, f)


    def get_functions(self):
        if self.debug:
            print(f"{__file__} - get_functions()")

        ret = []

        with open(self.config_file_name, "r") as f:
            data = json.loads(f.read())

            if "functions" in list(data.keys()):
                ret = data["functions"]
            else:
                if self.debug:
                    print(f"ERROR - {__file__} missing `functions` key in {self.config_file_name}")
        return ret

    def update_function(self, function_name: str, update: list):
        if self.debug:
            print(f"{__file__} - set_function_lines({function_name}, {update})")

        with open(self.config_file_name, "r") as f:
            data = json.loads(f.read())

            # always check if functions key exists
            if "functions" in list(data.keys()):

                if "name" in update.keys() and update["name"] in [n["name"] for n in data["conditions"]]:
                    if self.debug:
                        print(f"ERROR - {__file__} function `{update['name']}` already exists")
                else:
                    # and find place from array where to update function by name
                    for i, function in enumerate(data["functions"]):
                        if function["name"] == function_name:
                            data["functions"][i].update(update)
            else:
                if self.debug:
                    print(f"ERROR - {__file__} missing `functions` key in {self.config_file_name}")

        with open(self.config_file_name, "w") as f:
            json.dump(data, f)

    def get_function_lines(self, function_name: str):
        if self.debug:
            print(f"{__file__} - get_function_lines({function_name})")

        ret = []

        if function_name in [c["name"] for c in self.get_functions()]:
            ret = [c["lines"] for c in self.get_functions() if c["name"] == function_name][0]
        else:
            if self.debug:
                print(f"ERROR - {__file__} missing function `{function_name}` in {self.config_file_name}")

        return ret

    def add_new_condition(self, condition_name, enabled = False):
        if self.debug:
            print(f"{__file__} - add_new_condition({condition_name}, {enabled})")

        with open(self.config_file_name, "r") as f:
            data = json.loads(f.read())

            # first check if `condition` key exists
            # if not then create it
            if "conditions" in list(data.keys()):

                # also check if condition name exists already
                if condition_name not in [n["name"] for n in data["conditions"]]:
                    data["conditions"].append({"name": condition_name, "enabled": enabled, "lines": []})
                else:
                    if self.debug:
                        print(f"ERROR - {__file__} condition `{condition_name}` already exists")
            else:
                data["conditions"] = [{"name": condition_name, "enabled": enabled, "lines": []}]

        # and save that conf also
        with open(self.config_file_name, "w") as f:
            json.dump(data, f)

    def remove_condition(self, condition_name: str):
        if self.debug:
            print(f"{__file__} - remove_condition({condition_name})")

        with open(self.config_file_name, "r") as f:
            data = json.loads(f.read())

            # always check if functions key exists
            if "conditions" in list(data.keys()):
                for i, condition in enumerate(data["conditions"]):
                    if condition["name"] == condition_name:
                        data["conditions"].pop(i)
                        break
            else:
                if self.debug:
                    print(f"ERROR - {__file__} missing `conditions` key in {self.config_file_name}")

        with open(self.config_file_name, "w") as f:
            json.dump(data, f)


    def update_condition(self, condition_name: str, update: dict):
        if self.debug:
            print(f"{__file__} - update_condition({condition_name}, {update})")

        with open(self.config_file_name, "r") as f:
            data = json.loads(f.read())

            # always check if conditions key exists
            if "conditions" in list(data.keys()):

                # check if
                if "name" in update.keys() and update["name"] in [n["name"] for n in data["conditions"]]:
                    if self.debug:
                        print(f"ERROR - {__file__} condition `{update['name']}` already exists")
                else:
                    # and find place from array where to update condition by name
                    for i, condition in enumerate(data["conditions"]):
                        if condition["name"] == condition_name:
                            data["conditions"][i].update(update)
            else:
                if self.debug:
                    print(f"ERROR - {__file__} missing `conditions` key in {self.config_file_name}")

        with open(self.config_file_name, "w") as f:
            json.dump(data, f)


    def get_conditions(self):
        if self.debug:
            print(f"{__file__} - get_conditions()")

        ret = []

        try:
            with open(self.config_file_name, "r") as f:
                data = json.loads(f.read())

                # always check if `condition` key exists
                if "conditions" in list(data.keys()):
                    ret = data["conditions"]
                else:
                    if self.debug:
                        print(f"ERROR - {__file__} missing `conditions` key in {self.config_file_name}")
        except:
            return ret

        return ret

    def get_condition_lines(self, condition_name):
        if self.debug:
            print(f"{__file__} - get_condition_lines()")

        ret = []

        if condition_name in [c["name"] for c in self.get_conditions()]:
            ret = [c["lines"] for c in self.get_conditions() if c["name"] == condition_name][0]
        else:
            if self.debug:
                print(f"ERROR - {__file__} missing condition `{condition_name}` in {self.config_file_name}")

        return ret
