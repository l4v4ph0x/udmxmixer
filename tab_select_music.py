import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import *
import _thread
import time

from os import listdir
from os.path import isfile, isdir, join

from file_frame import file_frame

class tab_select_music:

    def __init__(self, window, ui):
        print(f"{__file__} - __init__({window}, {ui})")
        self.music_folder = file_frame().get_music_folder()
        self.selected_music = ""
        self.filter = None

        ui.btn_selecMusicFolder.clicked.connect(lambda: self.btn_selecMusicFolder_clicked(window, ui))
        ui.te_search.textChanged.connect(lambda: self.te_search_textChanged(window, ui))
        ui.lw_music.itemClicked.connect(lambda e: self.lw_music_itemClicked(e))

        if self.music_folder != "":
            _thread.start_new_thread(self.thread_update_lw_music, (window, ui))
        else:
            ui.btn_selecMusicFolder.setText("Select Music Folder")


    def thread_update_lw_music(self, window, ui):
        print(f"{__file__} - thread_update_lw_music({window}, {ui})")

        while True:
            self.fill_lw_music(window, ui, filter=self.filter, debug=False)
            time.sleep(1)

    def btn_selecMusicFolder_clicked(self, window, ui):
        print(f"{__file__} - btn_selecMusicFolder_clicked({window}, {ui})")

        music_folder = QFileDialog.getExistingDirectory(window, "select music folder")
        print(f"{__file__} - btn_selecMusicFolder_clicked: got folder `{music_folder}`")

        if not isdir(music_folder):
            print(f"ERROR - {__file__} `{self.music_folder}` is not folder")
            return

        self.music_folder = music_folder
        file_frame().set_music_folder(self.music_folder)
        self.fill_lw_music(window, ui)

    def te_search_textChanged(self, window, ui):
        print(f"{__file__} - te_search_textChanged({window}, {ui})")

        if ui.te_search.toPlainText() == "":
            self.filter = None
        else:
            self.filter = ui.te_search.toPlainText()

        self.fill_lw_music(window, ui, filter=self.filter)


    def lw_music_itemClicked(self, item):
        print(f"{__file__} - lw_music_itemClicked({item})")

        self.selected_music = item.text()
        print(f"{__file__} - lw_music_itemActivated: selected music {self.selected_music} ")

    def fill_lw_music(self, window, ui, filter = None, debug = True):
        if debug:
            print(f"{__file__} - fill_lw_music({window}, {ui})")

        ui.btn_selecMusicFolder.setText(self.music_folder)

        # get music list
        musics = [f for f in listdir(self.music_folder) if isfile(join(self.music_folder, f)) and join(self.music_folder, f).endswith("mp3")]
        if debug:
            print(f"{__file__} - btn_selecMusicFolder_clicked: got musics {musics}")

        # apply filter that contsins filter if its not none or null
        if filter != None:
            musics = [m for m in musics if filter.upper() in m.upper()]

        if ui.lw_music.count() != len(musics):
            ui.lw_music.clear()
            for i, music in enumerate(musics):
                ui.lw_music.insertItem(0, QListWidgetItem(music))
