# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'editCondition.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_editConditionDialog(object):
    def setupUi(self, editConditionDialog):
        editConditionDialog.setObjectName("editConditionDialog")
        editConditionDialog.resize(499, 300)
        self.lb_activateFunction = QtWidgets.QLabel(editConditionDialog)
        self.lb_activateFunction.setGeometry(QtCore.QRect(110, 70, 301, 16))
        self.lb_activateFunction.setObjectName("lb_activateFunction")
        self.lb_when = QtWidgets.QLabel(editConditionDialog)
        self.lb_when.setGeometry(QtCore.QRect(230, 150, 41, 16))
        self.lb_when.setObjectName("lb_when")
        self.cb_when = QtWidgets.QComboBox(editConditionDialog)
        self.cb_when.setGeometry(QtCore.QRect(10, 170, 481, 26))
        self.cb_when.setObjectName("cb_when")
        self.btn_save = QtWidgets.QPushButton(editConditionDialog)
        self.btn_save.setGeometry(QtCore.QRect(330, 260, 71, 32))
        self.btn_save.setObjectName("btn_save")
        self.btn_cancel = QtWidgets.QPushButton(editConditionDialog)
        self.btn_cancel.setGeometry(QtCore.QRect(400, 260, 91, 32))
        self.btn_cancel.setObjectName("btn_cancel")
        self.lb_atValue = QtWidgets.QLabel(editConditionDialog)
        self.lb_atValue.setGeometry(QtCore.QRect(220, 200, 61, 16))
        self.lb_atValue.setObjectName("lb_atValue")
        self.te_atValue = QtWidgets.QTextEdit(editConditionDialog)
        self.te_atValue.setGeometry(QtCore.QRect(20, 220, 461, 31))
        self.te_atValue.setObjectName("te_atValue")
        self.lb_conditionName = QtWidgets.QLabel(editConditionDialog)
        self.lb_conditionName.setGeometry(QtCore.QRect(200, 10, 101, 16))
        self.lb_conditionName.setObjectName("lb_conditionName")
        self.te_conditionName = QtWidgets.QTextEdit(editConditionDialog)
        self.te_conditionName.setGeometry(QtCore.QRect(20, 30, 461, 31))
        self.te_conditionName.setObjectName("te_conditionName")
        self.te_functions = QtWidgets.QTextEdit(editConditionDialog)
        self.te_functions.setGeometry(QtCore.QRect(20, 100, 461, 31))
        self.te_functions.setObjectName("te_functions")

        self.retranslateUi(editConditionDialog)
        QtCore.QMetaObject.connectSlotsByName(editConditionDialog)

    def retranslateUi(self, editConditionDialog):
        _translate = QtCore.QCoreApplication.translate
        editConditionDialog.setWindowTitle(_translate("editConditionDialog", "Dialog"))
        self.lb_activateFunction.setText(_translate("editConditionDialog", "Activate Function by name or splited by comma"))
        self.lb_when.setText(_translate("editConditionDialog", "when"))
        self.btn_save.setText(_translate("editConditionDialog", "Save"))
        self.btn_cancel.setText(_translate("editConditionDialog", "Cancel"))
        self.lb_atValue.setText(_translate("editConditionDialog", "At Value"))
        self.lb_conditionName.setText(_translate("editConditionDialog", "Condition Name"))

