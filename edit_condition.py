import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import *

from ui_editConditionDialog import Ui_editConditionDialog

from file_frame import file_frame
from conditions import conditions


class edit_condition:

    def __init__(self, condition_name):
        print(f"{__file__} - __init__()")

        self.condition_name = condition_name

        window = QDialog()
        window.ui = Ui_editConditionDialog()
        window.ui.setupUi(window)

        # set condition name graphically
        window.ui.te_conditionName.setText(self.condition_name)

        # fill combo boxes

        """
        window.ui.cb_activateFunction.addItem("Nothing")
        self.function_names = [] # add var that hold function names to choose

        for function in file_frame(debug=False).get_functions()[::-1]:
            window.ui.cb_activateFunction.addItem(function["name"])
            self.function_names.append(function["name"])

            for inner_function in [f for f in file_frame(debug=False).get_functions()[::-1] if f["name"] != function["name"]]:
                window.ui.cb_activateFunction.addItem(function["name"] + ", " + inner_function["name"])
                self.function_names.append(function["name"] + ", " + inner_function["name"])

                for inner_inner_function in [f for f in file_frame(debug=False).get_functions()[::-1] if f["name"] != function["name"] and f["name"] != inner_function["name"]]:
                    window.ui.cb_activateFunction.addItem(function["name"] + ", " + inner_function["name"] + ", " + inner_inner_function["name"])
                    self.function_names.append(function["name"] + ", " + inner_function["name"] + ", " + inner_inner_function["name"] )

                    for inner_inner_inner_function in [f for f in file_frame(debug=False).get_functions()[::-1] if f["name"] != function["name"] and f["name"] != inner_function["name"] and f["name"] != inner_inner_function["name"]]:
                        window.ui.cb_activateFunction.addItem(function["name"] + ", " + inner_function["name"] + ", " + inner_inner_function["name"] + ", " + inner_inner_inner_function["name"])
                        self.function_names.append(function["name"] + ", " + inner_function["name"] + ", " + inner_inner_function["name"] + ", " + inner_inner_inner_function["name"])
        """


        window.ui.cb_when.addItem("Nothing")
        for condition in conditions().get_conditions():
            window.ui.cb_when.addItem(condition)


        # set combo boxes selcted index

        condition_lines = file_frame().get_condition_lines(condition_name)

        """
        for l in condition_lines:
            if "function" in l.keys():
                # +1 cause first item is "Nothing"
                window.ui.cb_activateFunction.setCurrentIndex([f for f in self.function_names].index(l["function"]) +1)
                break
        """
        for l in condition_lines:
            if "function" in l.keys():
                window.ui.te_functions.setText(l["function"])
                break


        for l in condition_lines:
            if "when" in l.keys():
                # +1 cause first item is "Nothing"
                window.ui.cb_when.setCurrentIndex([c for c in conditions().get_conditions()].index(l["when"]) +1)
                break

        for l in condition_lines:
            if "at value" in l.keys():
                window.ui.te_atValue.setText(l["at value"])
                break

        # bottom buttons here
        window.ui.btn_save.clicked.connect(lambda: self.btn_save_clicked(window, window.ui))
        window.ui.btn_cancel.clicked.connect(lambda: self.btn_cancel_clicked(window, window.ui))

        window.exec_()

    def btn_save_clicked(self, window, ui):
        print(f"{__file__} - btn_cancel_clicked({window}, {ui})")

        settings = {}
        lines = []

        if ui.te_conditionName.toPlainText() != "" and ui.te_conditionName.toPlainText() != self.condition_name:
            settings["name"] = ui.te_conditionName.toPlainText()

        if ui.te_functions.toPlainText() != "":
            lines.append({"function": ui.te_functions.toPlainText()})

        if ui.cb_when.currentIndex() > 0:
            lines.append({"when": ui.cb_when.currentText()})

        if ui.te_atValue.toPlainText() != "":
            lines.append({"at value": ui.te_atValue.toPlainText()})

        settings["lines"] = lines
        file_frame().update_condition(self.condition_name, settings)

        window.accept()

    def btn_cancel_clicked(self, window, ui):
        print(f"{__file__} - btn_cancel_clicked({window}, {ui})")
        window.close()
