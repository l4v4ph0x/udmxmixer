class conditions:
    def __init__(self, debug = True):
        self.debug = debug

        if self.debug:
            print(f"{__file__} - __init__()")

        self.when_hit_master_level = "Hit Master Level Value"
        self.when_hit_hotkey = "When Hit Hotkey"

    def get_conditions(self):
        if self.debug:
            print(f"{__file__} - get_conditions()")

        return [self.when_hit_master_level, self.when_hit_hotkey]
