import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import *

from uDMX import uDMX

class tab_output_dmx:

    def __init__(self, window, ui):
        print(f"{__file__} - __init__({window}, {ui})")
        ui.chbx_udmx.stateChanged.connect(lambda e: self.chbx_udmx_stateChanged(e))
        self.udmx = None

    def chbx_udmx_stateChanged(self, state):
        print(f"{__file__} - chbx_udmx_stateChanged({state})")

        if state == 2:
            # also set number of channels we want to use
            self.udmx = uDMX(30)
        elif self.udmx is not None:
            self.udmx.close()

    def send_signal(self, start_address: int, channels: list, values: list, debug = True):
        if debug:
            print(f"{__file__} - send_signal({start_address}, {channels}, {values})")

        if self.udmx != None:
            self.udmx.set_start_address(start_address, debug=False)
            for i in range(0, len(channels)):
                self.udmx.set_channel_value("send 1", channels[i], values[i], debug=False)

            self.udmx.send("send 1", debug=debug)
        else:
            if debug:
                print(f"ERROR - {__file__} haven't set any udmx device")
