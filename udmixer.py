import sys
import os
import time
import _thread
from os import listdir
from os.path import isfile, isdir, join
from pynput import keyboard

# import tabs
from tab_output_dmx import tab_output_dmx
from tab_select_music import tab_select_music
from tab_functions import tab_functions
from tab_virtual_console import tab_virtual_console

# build uis
os.system("pyuic5 main.ui -o ui_mainDialog.py")
os.system("pyuic5 editCondition.ui -o ui_editConditionDialog.py")

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import *
from ui_mainDialog import Ui_mainDialog

from file_frame import file_frame
from conditions import conditions

output_dmx = None
select_music = None
functions = None
virtual_console = None

pessed_keys = []
already_running_functions = []
already_running_conditions = []

def thread_run_function_lines(condition_name, function_lines, at_value):
    global output_dmx, virtual_console, already_running_conditions

    #print(f"{__file__} - thread_run_function({function_lines}, {at_value})")

    for line in function_lines:
        channels = [int(s.replace(" ", "")) for s in line["channels"].split(",")]
        values = [int(s.replace(" ", "")) for s in  line["values"].split(",")]

        output_dmx.send_signal(int(line["start address"]), channels, values, debug=False)

        if line["time to hold"] == "above master":
            while virtual_console.play_music.is_playing() and virtual_console.play_music.player.bars >= int(at_value):
                time.sleep(0.01)
        else:
            time.sleep(int(line["time to hold"]) / 1000.0)

    already_running_conditions.pop(already_running_conditions.index(condition_name))


def thread_conditions_checker():
    global virtual_console, pessed_keys, already_running_functions, already_running_conditions

    print(f"{__file__} - thread_conditions_checker()")

    condition_function_counter = {}

    while True:
        function_name = ""
        when = ""
        at_value = 0

        for condition in [c for c in file_frame(debug=False).get_conditions() if c["enabled"] == True]:
            for l in condition["lines"]:
                if "when" in l.keys():
                    when = l["when"]

                if "function" in l.keys():
                    function_name = l["function"]

                if "at value" in l.keys():
                    at_value = l["at value"]

            if when == conditions(debug=False).when_hit_master_level:
                if virtual_console.play_music.is_playing():
                    if ", " in function_name:
                        if virtual_console.play_music.player.bars >= int(at_value):
                            if condition["name"] not in already_running_conditions:
                                function_names = function_name.split(", ")

                                if function_name in condition_function_counter.keys():
                                    if condition_function_counter[function_name] >= len(function_names) -1:
                                        condition_function_counter[function_name] = 0
                                    else:
                                        condition_function_counter[function_name] += 1
                                else:
                                    condition_function_counter[function_name] = 0

                                i = condition_function_counter[function_name]
                                function_lines = file_frame(debug=False).get_function_lines(function_names[i])
                                _thread.start_new_thread(thread_run_function_lines, (condition["name"], function_lines, at_value))

                                already_running_conditions.append(condition["name"])

                    else:
                        if virtual_console.play_music.player.bars >= int(at_value):
                            if function_name not in already_running_conditions:
                                function_lines = file_frame(debug=False).get_function_lines(function_name)
                                _thread.start_new_thread(thread_run_function_lines, (condition["name"], function_lines, at_value))

                                already_running_conditions.append(condition["name"])

            elif when == conditions(debug=False).when_hit_hotkey:
                if str(at_value) in pessed_keys:

                    if function_name not in already_running_functions:
                        if output_dmx != None:
                            already_running_functions.append(function_name)
                            function_lines = file_frame(debug=False).get_function_lines(function_name)
                            _thread.start_new_thread(thread_run_function_lines, (function_name, function_lines, at_value))
                elif function_name in already_running_functions:
                    already_running_functions.pop(already_running_functions.index(function_name))

        time.sleep(0.01)

def on_press(key):
    global pessed_key

    # single-char keys
    try: k = key.char
    # other keys
    except: k = key.name

    pessed_keys.append(str(k));
    print("pressed key: " + k)

def on_release(key):
    global pessed_key

    # single-char keys
    try: k = key.char
    # other keys
    except: k = key.name
    #print("released key: " + k)
    pessed_keys.pop(pessed_keys.index(str(k)))

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = QDialog()
    ui = Ui_mainDialog()
    ui.setupUi(window)

    # init our tabs
    output_dmx = tab_output_dmx(window, ui)
    select_music = tab_select_music(window, ui)
    functions = tab_functions(window, ui)
    virtual_console = tab_virtual_console(window, ui, select_music)

    _thread.start_new_thread(thread_conditions_checker, ())
    lis = keyboard.Listener(on_press=on_press, on_release=on_release)
    lis.start()

    window.show()
    sys.exit(app.exec_())
