import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import *

from os.path import isfile, isdir, join
import _thread
import time

from file_frame import file_frame
from play_music import play_music
from edit_condition import edit_condition

class tab_virtual_console:

    def __init__(self, window, ui, select_music):
        print(f"{__file__} - __init__({window}, {ui}, {select_music})")

        # set local vars
        self.select_music = select_music
        self.play_music = play_music()

        # set table widget headers
        ui.tw_conditions.setColumnCount(5)
        ui.tw_conditions.setHorizontalHeaderLabels(["Name", "Enabled", "Edit", "Remove", "switch only me"])

        ui.btn_play.clicked.connect(lambda: self.btn_play_clicked(window, ui))
        ui.btn_pause.clicked.connect(lambda: self.btn_pause_clicked(window, ui))
        ui.btn_stop.clicked.connect(lambda: self.btn_stop_clicked(window, ui))

        ui.btn_addCondition.clicked.connect(lambda: self.btn_addCondition_clicked(window, ui))

        # load conditions table widget
        conditions = file_frame().get_conditions()
        for i, condition in enumerate(conditions):
            self.add_condition_to_table_widget(window, ui, i, condition)


    def thread_music_playing(self, window, ui):
        print(f"{__file__} - thread_music_playing()")

        # first wait until player gets created
        while not self.play_music.is_playing():
            time.sleep(0.1)

        # set horizontal slider vals
        ui.hs_musicTime.setMaximum(int(self.play_music.player.length))
        max_time = round(self.play_music.player.length, 2)

        # then wait until its gets stopped or destroyed
        while self.play_music.is_playing():

            # set label and horizontal scroll values

            ui.hs_musicTime.setValue(int(self.play_music.player.time))

            percent = round(self.play_music.player.time * 100 / self.play_music.player.length, 2)
            seconds = round(self.play_music.player.time, 2)

            ui.lb_musicStatus.setText(f"{percent}% / 100% - {seconds}s / {max_time}")

            # set vetical progress bar value
            # to show music playing level in current milisecond
            #ui.vs_master.setValue(self.play_music.player.bars if self.play_music.player.bars <= 30 else 30)

            # sleep a little
            # so it thread wont over load or take too much resources
            time.sleep(0.1)

        # after player has shut down or stopped then
        # set label and horizontal scroll to 0
        ui.hs_musicTime.setValue(0)
        ui.lb_musicStatus.setText(f"0% / 100% - 0s / ...")

        print(f"{__file__} - thread_music_playing: ended it's work")

    def btn_play_clicked(self, window, ui):
        print(f"{__file__} - btn_play_clicked({window}, {ui})")

        if not isfile(self.select_music.music_folder + "/" + self.select_music.selected_music):
            print(f"ERROR - {__file__} music not found `{self.select_music.selected_music}` in `{self.select_music.music_folder}`")
            return

        if self.play_music == None:
            print(f"ERROR - {__file__} self.play_music is None")
            return

        self.play_music.play(self.select_music.music_folder + "/" + self.select_music.selected_music)

        # player wont start right away
        # and we can use that to check if were already playing music
        if not self.play_music.is_playing():
            _thread.start_new_thread(self.thread_music_playing, (window, ui))


    def btn_pause_clicked(self, window, ui):
        print(f"{__file__} - btn_pause_clicked({window}, {ui})")

        if self.play_music == None:
            print(f"ERROR - {__file__} self.play_music is None")
            return

        self.play_music.pause()

    def btn_stop_clicked(self, window, ui):
        print(f"{__file__} - btn_stop_clicked({window}, {ui})")

        if self.play_music == None:
            print(f"ERROR - {__file__} self.play_music is None")
            return

        self.play_music.stop()

    def add_condition_to_table_widget(self, window, ui, row, condition):
        print(f"{__file__} - add_condition_to_table_widget({window}, {ui}, {row}, {condition})")

        cb = QCheckBox()
        cb.setCheckState(2 if condition["enabled"] else 0)
        cb_switch_only_me = QCheckBox()

        btn_edit = QPushButton("Edit")
        btn_remove = QPushButton("Remove")

        cb.stateChanged.connect(lambda x: self.cb_enable_condition_stateChanged(window, ui, condition["name"], x))
        cb_switch_only_me.stateChanged.connect(lambda x: self.cb_switch_only_me_stateChanged(window, ui, condition["name"], x))
        btn_edit.clicked.connect(lambda: self.btn_edit_clicked(window, ui, condition["name"]))
        btn_remove.clicked.connect(lambda: self.btn_remove_clicked(window, ui, condition["name"]))

        ui.tw_conditions.setRowCount(ui.tw_conditions.rowCount() + 1)

        ui.tw_conditions.setItem(row, 0, QTableWidgetItem(condition["name"]))
        ui.tw_conditions.setCellWidget(row, 1, cb)
        ui.tw_conditions.setCellWidget(row, 2, btn_edit)
        ui.tw_conditions.setCellWidget(row, 3, btn_remove)
        ui.tw_conditions.setCellWidget(row, 4, cb_switch_only_me)


    def btn_addCondition_clicked(self, window, ui):
        print(f"{__file__} - btn_addCondition_clicked({window}, {ui})")

        for n in range(1, 100):
            if f"New Condition {n}" not in [f["name"] for f in file_frame().get_conditions()]:

                # add new condition
                file_frame().add_new_condition(f"New Condition {n}", True)

                # add default function line also
                #file_frame().set_condition_lines(f"New Condition {n}", [{"enabled": True}])

                # add it graphically also
                self.add_condition_to_table_widget(window, ui, ui.tw_conditions.rowCount(), file_frame().get_conditions()[-1])

                # so need to break not add anymore conditions
                break

    def cb_enable_condition_stateChanged(self, window, ui, condition_name, state):
        print(f"{__file__} - cb_enable_condition({window}, {ui}, {condition_name}, {state})")

        if state == 2:
            file_frame().update_condition(condition_name, {"enabled": True})
        else:
            file_frame().update_condition(condition_name, {"enabled": False})

    def cb_switch_only_me_stateChanged(self, window, ui, condition_name, state):
        print(f"{__file__} - cb_switch_only_me_stateChanged({window}, {ui}, {condition_name}, {state})")

        if state == 2:
            # disable all conditions thats not current one
            # and set check one to checked

            for row in range(0, ui.tw_conditions.rowCount()):
                if ui.tw_conditions.item(row, 0).text() == condition_name:
                    ui.tw_conditions.cellWidget(row, 1).setCheckState(2)
                else:
                    ui.tw_conditions.cellWidget(row, 1).setCheckState(0)


    def btn_edit_clicked(self, window, ui, condition_name):
        print(f"{__file__} - btn_edit_clicked({window}, {ui}, {condition_name})")
        edit_condition(condition_name)

        # reload conditions table widget
        ui.tw_conditions.setRowCount(0)

        conditions = file_frame().get_conditions()
        for i, condition in enumerate(conditions):
            self.add_condition_to_table_widget(window, ui, i, condition)

    def btn_remove_clicked(self, window, ui, condition_name):
        print(f"{__file__} - btn_remove_clicked({window}, {ui}, {condition_name})")

        file_frame().remove_condition(condition_name)

        # reload conditions table widget
        ui.tw_conditions.setRowCount(0)

        conditions = file_frame().get_conditions()
        for i, condition in enumerate(conditions):
            self.add_condition_to_table_widget(window, ui, i, condition)
