# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'lights.ui'
#
# Created by: PyQt5 UI code generator 5.12
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_lightsDialog(object):
    def setupUi(self, lightsDialog):
        lightsDialog.setObjectName("lightsDialog")
        lightsDialog.resize(233, 257)
        self.btn_done = QtWidgets.QPushButton(lightsDialog)
        self.btn_done.setGeometry(QtCore.QRect(10, 220, 211, 34))
        self.btn_done.setObjectName("btn_done")
        self.chbx_red1 = QtWidgets.QCheckBox(lightsDialog)
        self.chbx_red1.setGeometry(QtCore.QRect(10, 10, 61, 22))
        self.chbx_red1.setObjectName("chbx_red1")
        self.chbx_green1 = QtWidgets.QCheckBox(lightsDialog)
        self.chbx_green1.setGeometry(QtCore.QRect(80, 10, 71, 22))
        self.chbx_green1.setObjectName("chbx_green1")
        self.chbx_blue1 = QtWidgets.QCheckBox(lightsDialog)
        self.chbx_blue1.setGeometry(QtCore.QRect(160, 10, 61, 22))
        self.chbx_blue1.setObjectName("chbx_blue1")
        self.chbx_blue2 = QtWidgets.QCheckBox(lightsDialog)
        self.chbx_blue2.setGeometry(QtCore.QRect(160, 40, 61, 22))
        self.chbx_blue2.setObjectName("chbx_blue2")
        self.chbx_red2 = QtWidgets.QCheckBox(lightsDialog)
        self.chbx_red2.setGeometry(QtCore.QRect(10, 40, 61, 22))
        self.chbx_red2.setObjectName("chbx_red2")
        self.chbx_green2 = QtWidgets.QCheckBox(lightsDialog)
        self.chbx_green2.setGeometry(QtCore.QRect(80, 40, 71, 22))
        self.chbx_green2.setObjectName("chbx_green2")
        self.chbx_blue3 = QtWidgets.QCheckBox(lightsDialog)
        self.chbx_blue3.setGeometry(QtCore.QRect(160, 70, 61, 22))
        self.chbx_blue3.setObjectName("chbx_blue3")
        self.chbx_green3 = QtWidgets.QCheckBox(lightsDialog)
        self.chbx_green3.setGeometry(QtCore.QRect(80, 70, 71, 22))
        self.chbx_green3.setObjectName("chbx_green3")
        self.chbx_red3 = QtWidgets.QCheckBox(lightsDialog)
        self.chbx_red3.setGeometry(QtCore.QRect(10, 70, 61, 22))
        self.chbx_red3.setObjectName("chbx_red3")

        self.retranslateUi(lightsDialog)
        QtCore.QMetaObject.connectSlotsByName(lightsDialog)

    def retranslateUi(self, lightsDialog):
        _translate = QtCore.QCoreApplication.translate
        lightsDialog.setWindowTitle(_translate("lightsDialog", "Dialog"))
        self.btn_done.setText(_translate("lightsDialog", "Done"))
        self.chbx_red1.setText(_translate("lightsDialog", "Red1"))
        self.chbx_green1.setText(_translate("lightsDialog", "Green1"))
        self.chbx_blue1.setText(_translate("lightsDialog", "Blue1"))
        self.chbx_blue2.setText(_translate("lightsDialog", "Blue2"))
        self.chbx_red2.setText(_translate("lightsDialog", "Red2"))
        self.chbx_green2.setText(_translate("lightsDialog", "Green2"))
        self.chbx_blue3.setText(_translate("lightsDialog", "Blue3"))
        self.chbx_green3.setText(_translate("lightsDialog", "Green3"))
        self.chbx_red3.setText(_translate("lightsDialog", "Red3"))


